/*===========================================================================
  Copyright (C) 2011-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.longhornapi.impl.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import jakarta.xml.bind.JAXBException;
import net.sf.okapi.lib.longhornapi.impl.rest.transport.XMLStringList;

public class Util {

	public static URI createProject(URI baseUri) throws URISyntaxException, HttpException, IOException {
		HttpClient client = new DefaultHttpClient();
		
		HttpPost postMethod = new HttpPost(baseUri + "/projects/new");
		HttpResponse response = client.execute(postMethod);
		Header[] projectUri = response.getHeaders("Location");
		postMethod.releaseConnection();
		URI projUri = new URI(projectUri[0].getValue());
		return projUri;
	}

	public static void put(String uri, MultipartEntity params) throws IOException {
		HttpClient client = new DefaultHttpClient();
		
		HttpPut putMethod = new HttpPut(uri);
		putMethod.setEntity(params);
		HttpResponse response = client.execute(putMethod);
		EntityUtils.consume(response.getEntity());
	}

	public static void delete(String uri) throws IOException {
		HttpClient client = new DefaultHttpClient();
		
		HttpDelete delMethod = new HttpDelete(uri);
		HttpResponse response = client.execute(delMethod);
		EntityUtils.consume(response.getEntity());
	}

	public static void post(String uri, MultipartEntity params) throws IOException {
		HttpClient client = new DefaultHttpClient();
		
		HttpPost postMethod = new HttpPost(uri);
		if (params != null)
			postMethod.setEntity(params);
		
		HttpResponse response = client.execute(postMethod);
		try {
			int status = response.getStatusLine().getStatusCode();
			if (status == HttpStatus.SC_INTERNAL_SERVER_ERROR)
				throw new RuntimeException(EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8));
		}
		finally {
			EntityUtils.consume(response.getEntity());
		}
	}

	public static ArrayList<String> getList(String uri) throws IOException, JAXBException {
		HttpClient client = new DefaultHttpClient();

		HttpGet getMethod = new HttpGet(uri);
		HttpResponse response = client.execute(getMethod);
		try {
			String xmlList = EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8);

			return XMLStringList.unmarshal(xmlList);
		}
		finally {
			EntityUtils.consume(response.getEntity());
		}
	}
}
