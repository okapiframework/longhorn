#### About Okapi Longhorn ####

The **Okapi Longhorn** project allows you to execute batch configurations remotely.

For more information about Okapi Longhorn, see the [corresponding page on the main wiki](http://okapiframework.org/wiki/index.php?title=Longhorn).

Bug report and enhancement requests: https://bitbucket.org/okapiframework/longhorn/issues

#### Build status: ####

`dev` branch: [![pipeline status](https://gitlab.com/okapiframework/longhorn/badges/dev/pipeline.svg)](https://gitlab.com/okapiframework/longhorn/commits/dev)

`master` branch: [![pipeline status](https://gitlab.com/okapiframework/longhorn/badges/master/pipeline.svg)](https://gitlab.com/okapiframework/longhorn/commits/master)

#### Downloads: ####

The latest stable version of Okapi Longhorn is at https://okapiframework.org/binaries/longhorn/  
The nightly version of Okapi Longhorn is at https://gitlab.com/okapiframework/longhorn/-/jobs/artifacts/dev/browse/deployment/done?job=verification

#### Developing with the API ####

Java bindings for the Longhorn REST API are available as a maven artifact.
The release artifacts are available in Maven Central, so all you need to do
is add the dependency to your pom.xml:

```xml
  <!-- .... -->
  <dependencies>
    <dependency>
      <groupId>net.sf.okapi.lib</groupId>
      <artifactId>okapi-lib-longhorn-api</artifactId>
      <version>1.47.0</version>
    </dependency>
  </dependencies>
```

To develop with the latest nightly snapshot build, you also need to add a custom repository:

```xml
  <repositories>
    <repository>
      <id>okapi-snapshot</id>
      <name>Okapi Snapshot</name>
      <url>https://oss.sonatype.org/content/repositories/snapshots/</url>
    </repository>
  </repositories>
  <!-- .... -->
  <dependencies>
    <dependency>
      <groupId>net.sf.okapi.lib</groupId>
      <artifactId>okapi-lib-longhorn-api</artifactId>
      <version>1.48.0-SNAPSHOT</version>
    </dependency>
  </dependencies>
```
